﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mytest0505.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["a"] = "123";
            ViewBag.Name = "jay";

            ViewData["a"] = 1;
            ViewData["b"] = 2;

            ViewBag.a = 1;
            ViewBag.b = 2;
            return View();
        }

        public ActionResult Html()
        {
            return View();
        }

        public ActionResult Htmlhelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}