﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mytest0505.ViewModel;


namespace mytest0505.Controllers
{
    public class BMIController : Controller
    {

        
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (ModelState.IsValid)
            {
                var m_height = data.Height / 100;
                var result = data.Weight / (m_height * m_height);
                var level = "";
                
                if (result <18.5)
                {
                    level = "體重過輕";
                }
                else if (result >=18.5 && result <24)
                {
                    level = "正常範圍";
                }   
                else if (result >= 24 && result <27)
                {
                    level = "過重";
                }
                else if (result >=27 && result <30)
                {
                    level = "輕度肥胖";
                }
                else if (result >= 30 && result <35)
                {
                    level = "中度肥胖";
                }
                else if (result >= 35)
                {
                    level = "重度肥胖";
                }
                data.Result = result;
                data.Level = level;
               
               
            }
            

            return View(data);
        }
    }
}